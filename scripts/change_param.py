#!/usr/bin/env python

# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
import sys, os
from beginner_tutorials.srv import *

'''def handle_add_two_ints(req):
    print "Returning [%s + %s = %s]"%(req.a, req.b, (req.a + req.b))
    return AddTwoIntsResponse(req.a + req.b)

def add_two_ints_server():
    rospy.init_node('add_two_ints_server')
    s = rospy.Service('add_two_ints', AddTwoInts, handle_add_two_ints)
    print "Ready to add two ints."
    rospy.spin()
'''

def handle_change(dim):
	height = dim.a
	width = dim.b
	return(height, width)

def change_server():
	rospy.init_node('change_param_server')
	suc = rospy.ServiceProxy('Height_Width_Parameters', height_width, handle_change)
        response = suc(height, width)
        print("ReadY !")
	rospy.spin()

if __name__ == "__main__":
    change_server()
