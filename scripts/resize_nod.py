#!/usr/bin/env python

# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
import sys, os
from beginner_tutorials.srv import *
import PIL
import scipy.misc

# Instantiate CvBridge
bridge = CvBridge()
#default height width

encoding = "bgr8"
image_topic = "image_publish/cv_bridge"
#pub = rospy.Publisher('image_publish/cv_bridge/resized_image', Image, queue_size=10)

class Resizer():
	
	def __init__(self):
	    self._height = 320
            self._width = 230

	def image_resize(self, msg):
	    try:
		pass
		cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
		resized_img = cv2.resize(cv2_img, (height, width))
	    except CvBridgeError, e:
		print(e)
	    else:
		
		cv2.waitKey(1)
		pub = rospy.Publisher('image_publish/cv_bridge/resized_image', Image, queue_size=10)
		res_img = scipy.misc.imresize(resized_img, (self._height, self._width))
		resize_img = bridge.cv2_to_imgmsg(res_img, encoding="bgr8")
		pub.publish(resize_img)
		rate = rospy.Rate(60)
		rate.sleep()
	
	def set_resolution(self, res):
  		self._height = res.height
		self._width = res.width
		return True
 	
def main():
    
    #Inital values of height and width
    rospy.init_node('Resize_node')
    rs = Resizer()
    #rospy.wait_for_service('Height_Width_Parameters') 
    change_size = rospy.ServiceProxy('Height_Width_Parameters', height_width) 
    sub = rospy.Subscriber(image_topic, Image, rs.image_resize)
   
    s = rospy.Service('Height_Width_Parameters', height_width, rs.set_resolution)
    #change_size = rospy.ServiceProxy('Height_Width_Parameters', height_width)
    pub = rospy.Publisher('image_publish/cv_bridge/resized_image', Image, queue_size=10)
    rospy.spin()

if __name__ == '__main__':
    
	height = rospy.get_param("/image_size/resize_nod/height")
	width = rospy.get_param("/image_size/resize_nod/width")	
	main()
