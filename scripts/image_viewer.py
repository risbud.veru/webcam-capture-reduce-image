#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys, os


# Instantiate CvBridge
bridge = CvBridge()
#bridge_2 = CvBridge()

def image_callback(msg):
    print("Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
    except CvBridgeError, e:
        print(e)
    else:
        # Save your OpenCV2 image as a jpeg 
        cv2.imshow('camera_image.jpeg', cv2_img)
	cv2.waitKey(1) #Important to show delay between frames

def main():
    rospy.init_node('Image_Viewer')
    # Define your image topic
    image_topic = "image_publish/cv_bridge"
    #image_topic = "image_publish/cv_bridge/resized_image"
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, image_callback)
    #rospy.Subscriber()
    # Spin until ctrl + c
    rospy.Subscriber(image_topic, Image) #image_fallback)
    rospy.spin()

if __name__ == '__main__':
    main()
