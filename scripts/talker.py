#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import Image
from PIL import Image
import os, sys

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    #while not rospy.is_shutdown():
    	#hello_str = "You're gonna die" 
    #	pub.publish("Image accepted")   	
    #	rospy.loginfo(hello_str)
    #	pub.publish(hello_str)
    #	rate.sleep()
    pub.publish("Published")

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
