#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import Image
from PIL import Image
import os, sys

def image_read():

	pub = rospy.Publisher('sensor_msgs/Image', String, queue_size=10)
    	rospy.init_node('reader_node', anonymous=True)
    	while not rospy.is_shutdown():
        	pub.publish("I am alive")

if __name__ == '__main__':
    try:
        image_read()
    except rospy.ROSInterruptException:
        pass
