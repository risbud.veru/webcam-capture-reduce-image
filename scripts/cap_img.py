#! /usr/bin/python

from __future__ import print_function

import roslib
##roslib.load_manifest('my_package')
from sensor_msgs.msg import Image
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


class image_converter:

	def __init__(self):
    		self.image_pub = rospy.Publisher("image_topic_publish",Image,queue_size = 10)
    		self.bridge = CvBridge()
    		self.image_sub = rospy.Subscriber("image_topic_subscribe",Image,self.callback)
    		rospy.loginfo("Initalization is done")
		print("Initalization is done")
    		#Creates publisher, subscriber and creates bridge object

  	def callback(self,data):
		print("CALLBACK IS HERE")
    		rospy.loginfo("Now in callback")
    		try:
    			cv_image = self.bridge.imgmsg_to_cv2(img, "bgr8")#data, "bgr8")
			self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
			rospy.loginfo("Image converted")
    		except CvBridgeError as e:
    			rospy.loginfo(e)

	def i_am_vir(self):
		print("CALLBACK IS HERE")
    		rospy.loginfo("Now in callback")
    		try:
			pub = rospy.Publisher('image_publish', Image, queue_size=None)
    			rospy.init_node('img_pub', anonymous=True)
    			rate = rospy.Rate(60) # 60hz

    			cam = cv2.VideoCapture(0)
    			bridge = CvBridge()

    			if not cam.isOpened():
         			sys.stdout.write("Webcam is not available")
         			return -1

    			while not rospy.is_shutdown():
        			ret, frame = cam.read()
        			msg = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
	
        			pub.publish(msg)
        			rate.sleep()
				rospy.loginfo("Image converted")
    		except CvBridgeError as e:
    			rospy.loginfo(e)

    #(rows,cols,channels) = cv_image.shape
    #if cols > 60 and rows > 60 :
    #	cv2.circle(cv_image, (50,50), 10, 255)

    #cv2.imshow("Image window", cv_image)
    #cv2.waitKey(3)

    		try:
    			self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    		except CvBridgeError as e:
      			print(e)
def main(args):
	rospy.loginfo("Object creating...")
	ic = image_converter()
	rospy.loginfo("Object created")
  	rospy.init_node("img_conv", anonymous=True)
  	rospy.loginfo("Node running now !!")
  	try:
    		rospy.loginfo("going to callback now ")
    		#rospy.spin()
		ic.i_am_vir()
  	except KeyboardInterrupt:
    		rospy.loginfo("Shutting down")
  		cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
